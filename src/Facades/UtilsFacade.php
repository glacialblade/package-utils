<?php namespace Glacialblade\Utils\Facades;

use Illuminate\Support\Facades\Facade;

class UtilsFacade extends Facade {

	protected static function getFacadeAccessor() {
		return 'Utils';
	}

}