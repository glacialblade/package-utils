<?php namespace Glacialblade\Utils\Facades;

use Glacialblade\Utils\Libraries\DateUtils;
use Glacialblade\Utils\Libraries\ObjectUtils;
use Glacialblade\Utils\Libraries\OtherUtils;
use Glacialblade\Utils\Libraries\StringUtils;

class UtilsFacadeFunctions {

	public static function date() {
		return new DateUtils();
	}

	public static function object() {
		return new ObjectUtils();
	}

	public static function string() {
		return new StringUtils();
	}

	public static function other() {
		return new OtherUtils();
	}

}