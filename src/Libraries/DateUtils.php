<?php namespace Glacialblade\Utils\Libraries;

use DateTime;

class DateUtils {

	/**
	 * Format a Date.
	 * ex: formatDate('2015/01/30 12:00:00', 'Y/m/d H:i:s', 'Y-m-d H:i:s')
	 *
	 * @param $date
	 * @param $from
	 * @param $to
	 *
	 * @return bool|string
	 */
	public static function format($date, $from, $to) {
		$date = DateTime::createFromFormat($from, $date);
		return $date->format($to);
	}

	/**
	 * Calculates the Age
	 * @param $birthDate
	 *
	 * @return int
	 */
	public static function age($birthDate) {
		$date = new DateTime($birthDate);
		$now  = new DateTime();
		$interval = $now->diff($date);
		return $interval->y;
	}

	/**
	 * Subract a Date.
	 * Intervals are y, m, d, h, i, s
	 * @param $date1
	 * @param $date2
	 */
	public static function subtract($date1, $date2, $interval) {
		$date1 = new DateTime($date1);
		$date2 = new DateTime($date2);

		$result = $date1->diff($date2);
		return $result->$interval;
	}

}