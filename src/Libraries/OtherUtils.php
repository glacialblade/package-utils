<?php namespace Glacialblade\Utils\Libraries;

class OtherUtils {

	/**
	 * Includes CSV Header
	 * @param $filename
	 */
	public static function includeCSVHeader($filename) {
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Expires: 0");
	}

}