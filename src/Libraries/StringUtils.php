<?php namespace Glacialblade\Utils\Libraries;

use Illuminate\Support\Facades\DB;

class StringUtils {

	/**
	 * DB Escapes
	 *
	 * @param $string
	 *
	 * @return $string
	 */
	public static function escape($string) {
		return DB::connection()->getPdo()->quote($string);
	}

	/**
	 * Remove New Lines \n from a string
	 * @param $string
	 *
	 * @return string
	 */
	public static function removeNewLines($string) {
		return trim(preg_replace('/\s+/', ' ', $string));
	}

	/**
	 * Fix Data Table Row Structure
	 * @param $array
	 * @param $keys
	 *
	 * @return array
	 */
	public static function fixDataTableRows($array, $keys) {
		$rows = [];

		foreach($array as $index=>$row) {
			foreach($keys as $key) {
				if(isset($row[$key])) {
					if($key == 'created_at') {
						$rows[$index][] = (String) clone $row[$key];
					}
					else {
						$rows[$index][] = $row[$key];
					}
				}
				else {
					$rows[$index][] = '';
				}
			}
		}

		return $rows;
	}

	/**
	 * Generate UUID
	 *
	 * @return array
	 */
	public static function uuid() {
		$uuid = array(
			'time_low'  => 0,
			'time_mid'  => 0,
			'time_hi'  => 0,
			'clock_seq_hi' => 0,
			'clock_seq_low' => 0,
			'node'   => array()
		);

		$uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
		$uuid['time_mid'] = mt_rand(0, 0xffff);
		$uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
		$uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
		$uuid['clock_seq_low'] = mt_rand(0, 255);

		for ($i = 0; $i < 6; $i++) {
			$uuid['node'][$i] = mt_rand(0, 255);
		}

		$uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
			$uuid['time_low'],
			$uuid['time_mid'],
			$uuid['time_hi'],
			$uuid['clock_seq_hi'],
			$uuid['clock_seq_low'],
			$uuid['node'][0],
			$uuid['node'][1],
			$uuid['node'][2],
			$uuid['node'][3],
			$uuid['node'][4],
			$uuid['node'][5]
		);

		return $uuid;
	}

	/**
	 * Removes all Special Characters.
	 *
	 * @return mixed
	 */
	public function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	/**
	 * Generates random string
	 * @param int $length
	 *
	 * @return string
	 */
	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}