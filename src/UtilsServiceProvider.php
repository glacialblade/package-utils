<?php namespace Glacialblade\Utils;

use Glacialblade\Modular\Console\Commands\ModularConfigCommand;
use Illuminate\Support\ServiceProvider;

class UtilsServiceProvider extends ServiceProvider {

	public function boot() {

	}

	public function register() {
		$this->registerFacades();
	}

	protected function registerFacades() {
		$this->app->bind('Utils', 'Glacialblade\Utils\Facades\UtilsFacadeFunctions');
	}

}